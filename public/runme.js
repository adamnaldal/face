
function setup() {
  frameRate(60)
  createCanvas(windowWidth, windowHeight)
  colorMode(HSB, 360, 100, 100, 100)
  angleMode(DEGREES);
  strokeWeight(1);

}

function draw() {
  background(80);
  textSize(30);

  noFill();
  beginShape();
  //chin
  curveVertex(500, 500);
  curveVertex(500, 500);

  //jaw
  curveVertex(580, 440);

  //temple
  curveVertex(590, 300);

  //forehead
  curveVertex(540, 230);
  curveVertex(460, 230);

  // temple
  curveVertex(410, 300);

  //jaw
  curveVertex(420, 440);

  //chin
  curveVertex(500, 500);
  curveVertex(500, 500);
  endShape();

  push();
  // pupils
  fill(0,0,0,100);
  circle(540, 330, 8);
  circle(460, 330, 8);


  //nose
  line(500, 350, 485, 390)
  line(485, 390, 500, 390)

  //triangle
  triangle(500, 470-30, 460, 460-30, 540, 460-30)
  pop();
}
